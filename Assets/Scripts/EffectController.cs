﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectController : MonoBehaviour
{
    public static EffectController Instance;

    public GameObject goodEffect, reallyGoodEffect, perfectEffect;
    public GameObject Spr_goodEffect, Spr_reallyGoodEffect, Spr_perfectEffect, Spr_closeEffect, Spr_missEffect;

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
    }

    public void GoodEffect()
    {
        Vector3 position = new Vector3(Random.Range(-4.5f, -2.25f), Random.Range(-3.3f, -4.75f));
        GameObject sprite = Instantiate(Spr_goodEffect, position, Spr_goodEffect.transform.rotation);
        GameObject particle = Instantiate(goodEffect, transform.position, goodEffect.transform.rotation);
        Destroy(particle, 1f);
        Destroy(sprite, 0.8f);
    }

    public void ReallyGoodEffect()
    {
        Vector3 position = new Vector3(Random.Range(-4.5f, -2.25f), Random.Range(-3.3f, -4.75f));
        GameObject sprite = Instantiate(Spr_reallyGoodEffect, position, Spr_reallyGoodEffect.transform.rotation);
        GameObject particle = Instantiate(reallyGoodEffect, transform.position, reallyGoodEffect.transform.rotation);
        Destroy(particle, 1f);
        Destroy(sprite, 0.8f);
    }

    public void PerfectEffect()
    {
        Vector3 position = new Vector3(Random.Range(-4.5f, -2.25f), Random.Range(-3.3f, -4.75f));
        GameObject sprite = Instantiate(Spr_perfectEffect, position, Spr_perfectEffect.transform.rotation);
        GameObject particle = Instantiate(perfectEffect, transform.position, perfectEffect.transform.rotation);
        Destroy(particle, 1f);
        Destroy(sprite, 0.8f);
    }

    public void CloseEffect()
    {
        Vector3 position = new Vector3(Random.Range(-4.5f, -2.25f), Random.Range(-3.3f, -4.75f));
        GameObject clone = Instantiate(Spr_closeEffect, position, Spr_closeEffect.transform.rotation);
        Destroy(clone, 0.8f);
    }

    public void MissEffect()
    {
        Vector3 position = new Vector3(Random.Range(-4.5f, -2.25f), Random.Range(-3.3f, -4.75f));
        GameObject clone = Instantiate(Spr_missEffect, position, Spr_missEffect.transform.rotation);
        Destroy(clone, 0.8f);
    }
}
