﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteSpecialPower : MonoBehaviour
{
    public bool canBePressed;

    public KeyCode keyToPress;

    private void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(keyToPress))
        {
            if (canBePressed)
            {
                if (GameManager.Instance.specialAct)
                {
                    CameraMov.Instance.takeDmg = false;
                    CameraMov.Instance.movCamera = true;

                    //Debug.Log("NextStage");
                    if (GameManager.Instance.numberStage == 4)
                    {
                        Gameplay_UI.Instance.MoveSliderPowerBoss();
                    }
                    else
                    {
                        Gameplay_UI.Instance.MoveSliderPower();
                    }
                    GameManager.Instance.numberStage++;
                    gameObject.SetActive(false);
                }
            }
        }
    }

    IEnumerator EsperarEjecutar(float tiempo, Action accion)
    {
        yield return new WaitForSecondsRealtime(tiempo);
        accion();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Activator")
        {
            canBePressed = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Activator" && gameObject.activeSelf)
        {
            canBePressed = false;
            GameManager.Instance.NoteMissed();
            EffectController.Instance.MissEffect();
            StartCoroutine(EsperarEjecutar(3f, RestartScene));
            //Debug.Log("No activaste el especial");
        }
    }

    void RestartScene()
    {
        LevelManager.Instance.Lose();
    }
}
