﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonController : MonoBehaviour
{
    private SpriteRenderer sprR;
    public Sprite defaultImage;
    public Sprite pressedImage;

    // Start is called before the first frame update
    void Start()
    {
        sprR = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            sprR.sprite = pressedImage;
        }

        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            sprR.sprite = defaultImage;
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            sprR.sprite = pressedImage;
        }

        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            sprR.sprite = defaultImage;
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            sprR.sprite = pressedImage;
        }

        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            sprR.sprite = defaultImage;
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            sprR.sprite = pressedImage;
        }

        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            sprR.sprite = defaultImage;
        }

        if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            sprR.sprite = pressedImage;
        }

        if (Input.GetKeyUp(KeyCode.KeypadEnter))
        {
            sprR.sprite = defaultImage;
        }
    }
}
