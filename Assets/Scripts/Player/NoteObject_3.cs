﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteObject_3 : MonoBehaviour
{
    public bool canBePressed;

    public KeyCode keyToPress;

    public bool nearNotes;
    public GameObject nearNote;

    // Start is called before the first frame update
    void Start()
    {
        if (nearNotes)
        {
            nearNote.GetComponent<NoteObject_3>().enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(keyToPress))
        {
            if (canBePressed)
            {
                if (!nearNotes)
                {
                    gameObject.SetActive(false);
                }
                else
                {
                    gameObject.SetActive(false);
                    nearNote.GetComponent<NoteObject_3>().enabled = true;
                }

                if (transform.position.y < -4.2f)
                {
                    Gameplay_UI.Instance.IncrementProgress(GameManager.Instance.goodValor);
                    GameManager.Instance.NoteHit(4);
                    EffectController.Instance.GoodEffect();
                    //Debug.Log("Good");
                }
                else if (transform.position.y < -3.9f)
                {
                    Gameplay_UI.Instance.IncrementProgress(GameManager.Instance.reallyGoodValor);
                    GameManager.Instance.NoteHit(3);
                    EffectController.Instance.ReallyGoodEffect();
                    //Debug.Log("Really Good");
                }
                else if (transform.position.y < -3.7f)
                {
                    Gameplay_UI.Instance.IncrementProgress(GameManager.Instance.perfectValor);
                    GameManager.Instance.NoteHit(2);
                    EffectController.Instance.PerfectEffect();
                    //Debug.Log("Perfect");
                }
                else if (transform.position.y > -3.7f)
                {
                    Gameplay_UI.Instance.IncrementProgress(GameManager.Instance.mehValor);
                    GameManager.Instance.NoteHit(1);
                    EffectController.Instance.CloseEffect();
                    //Debug.Log("Meh");
                }
                GameManager.Instance.presionoTecla = true;
            }
        }
        if (Input.GetKeyUp(keyToPress))
        {
            GameManager.Instance.presionoTecla = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Activator")
        {
            canBePressed = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Activator" && gameObject.activeSelf)
        {
            canBePressed = false;
            GameManager.Instance.NoteMissed();
            EffectController.Instance.MissEffect();
        }
    }
}
