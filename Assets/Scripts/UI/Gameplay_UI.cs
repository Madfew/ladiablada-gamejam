﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gameplay_UI : MonoBehaviour
{
    public static Gameplay_UI Instance;
    public Animator animSlider;

    public Text textDamage;

    public Slider sliderPower;

    public float fillSpeed = 0.5f;
    private float targetProgress = 0;

    private void Awake()
    {
        Instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        if (sliderPower.value > targetProgress)
        {
            sliderPower.value -= fillSpeed * Time.deltaTime;
        }

        if (sliderPower.value < targetProgress)
        {
            sliderPower.value += fillSpeed * Time.deltaTime;
        }

        //textDamage.text = GameManager.Instance.damage.ToString();
    }

    public void IncrementProgress(float newProgress)
    {
        targetProgress = sliderPower.value + newProgress;
    }

    public void DecreaseProgress(float newProgress)
    {
        targetProgress = sliderPower.value - newProgress;
    }

    public void ResetProgress()
    {
        targetProgress = 0;
    }

    public void MoveSliderPower()
    {
        animSlider.SetTrigger("move");
    }

    public void MoveSliderPowerBoss()
    {
        animSlider.SetTrigger("moveBoss");
    }
}
