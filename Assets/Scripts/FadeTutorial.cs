﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeTutorial : MonoBehaviour
{
    public SpriteRenderer[] tutorial;

    public float fadeSpeed = 1f;
    public bool a;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKeyDown)
        {
            a = true;
        }

        if (a)
        {
            Color fadeOut1 = tutorial[0].GetComponent<SpriteRenderer>().material.color;
            Color fadeOut2 = tutorial[1].GetComponent<SpriteRenderer>().material.color;
            Color fadeOut3 = tutorial[2].GetComponent<SpriteRenderer>().material.color;
            Color fadeOut4 = tutorial[3].GetComponent<SpriteRenderer>().material.color;

            float fadeAmount1 = fadeOut1.a - (fadeSpeed * Time.deltaTime);
            float fadeAmount2 = fadeOut2.a - (fadeSpeed * Time.deltaTime);
            float fadeAmount3 = fadeOut3.a - (fadeSpeed * Time.deltaTime);
            float fadeAmount4 = fadeOut4.a - (fadeSpeed * Time.deltaTime);

            fadeOut1 = new Color(fadeOut1.r, fadeOut1.g, fadeOut1.b, fadeAmount1);
            tutorial[0].GetComponent<SpriteRenderer>().material.color = fadeOut1;

            fadeOut2 = new Color(fadeOut2.r, fadeOut2.g, fadeOut2.b, fadeAmount2);
            tutorial[1].GetComponent<SpriteRenderer>().material.color = fadeOut1;

            fadeOut3 = new Color(fadeOut3.r, fadeOut3.g, fadeOut3.b, fadeAmount3);
            tutorial[2].GetComponent<SpriteRenderer>().material.color = fadeOut1;

            fadeOut4 = new Color(fadeOut4.r, fadeOut4.g, fadeOut4.b, fadeAmount4);
            tutorial[3].GetComponent<SpriteRenderer>().material.color = fadeOut1;

            if(fadeOut1.a <= 0)
            {
                a = false;
            }

        }
    }

}
