﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class LifeEnemy_UI : MonoBehaviour
{
    public static LifeEnemy_UI Instance;

    private Slider slider;

    public float fillSpeed = 0.5f;
    private float targetProgress = 1;

    private bool bossMuerto;

    private void Awake()
    {
        Instance = this;
        slider = gameObject.GetComponent<Slider>();
    }

    // Start is called before the first frame update
    void Start()
    {
        //DecreaseProgress(0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        if(slider.value > targetProgress)
        {
            slider.value -= fillSpeed * Time.deltaTime;
        }

        if(slider.value < 0.01f)
        {
            bossMuerto = true;
            if (bossMuerto)
            {
                bossMuerto = false;
                StartCoroutine(EsperarEjecutar(5.5f, Win));
            }
        }
    }

    public void DecreaseProgress(float newProgress)
    {
        targetProgress = slider.value - newProgress;
    }

    IEnumerator EsperarEjecutar(float tiempo, Action accion)
    {
        yield return new WaitForSecondsRealtime(tiempo);
        accion();
    }

    public void Win()
    {
        LevelManager.Instance.WinScene();
    }
}
