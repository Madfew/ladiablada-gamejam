﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManager : MonoBehaviour
{
    public static AnimationManager Instance;

    public Animator player;
    public Animator diablo;
    public Animator[] diablillo;

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.presionoTecla)
        {
            if (Input.GetKeyUp(KeyCode.LeftArrow))
            {
                AnimLateral();
            }
            if (Input.GetKeyUp(KeyCode.RightArrow))
            {
                AnimLateral();
            }
            if (Input.GetKeyUp(KeyCode.UpArrow))
            {
                AnimUp();
            }
            if (Input.GetKeyUp(KeyCode.DownArrow))
            {
                AnimDown();
            }
        }
    }

    void AnimUp()
    {
        player.SetTrigger("up");
    }

    void AnimDown()
    {
        player.SetTrigger("down");
    }

    void AnimLateral()
    {
        player.SetTrigger("lateral");
    }

}
