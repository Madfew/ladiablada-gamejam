﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance;
    public GameObject fade;

    void Start()
    {
        Instance = this;
    }

    public void RestartScene()
    {
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        SceneManager.LoadScene(1);
    }

    public void WinScene()
    {
        SceneManager.LoadScene(2);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void Lose()
    {
        SceneManager.LoadScene(3);
    }

    public void Gameplay()
    {
        fade.SetActive(true);
        StartCoroutine(EsperarEjecutar(1.25f, CargarEscenaGameplay));
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void CargarEscenaGameplay()
    {
        SceneManager.LoadScene(1);
    }

    IEnumerator EsperarEjecutar(float tiempo, Action accion)
    {
        yield return new WaitForSecondsRealtime(tiempo);
        accion();
    }

}
