﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CameraMov : MonoBehaviour
{
    public static CameraMov Instance;

    public GameObject cameraObj;
    public Vector3 targetPos;

    Vector3 velocity = new Vector3(0, 0, -10);
    float smoothTime = 0.3f;

    public bool movCamera = false;
    public bool takeDmg = false;

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        if (movCamera)
        {
            Vector3 targetPosition = new Vector3(cameraObj.transform.position.x, targetPos.y + 3, -10);
            cameraObj.transform.position = Vector3.SmoothDamp(cameraObj.transform.position, targetPosition, ref velocity, smoothTime);
            if (!takeDmg)
            {
                takeDmg = true;
                StartCoroutine(EsperarEjecutar(1f, DamageDiablo));
            }
            StartCoroutine(EsperarEjecutar(GameManager.Instance.camara, CameraReturn));
        }
    }

    IEnumerator EsperarEjecutar(float tiempo, Action accion)
    {
        yield return new WaitForSecondsRealtime(tiempo);
        accion();
    }

    void DamageDiablo()
    {
        AnimationManager.Instance.player.SetBool("attack", true);
        if (GameManager.Instance.numberStage == 5)
        {
            AnimationManager.Instance.diablo.SetBool("death", true);
        }
        else
        {
            AnimationManager.Instance.diablo.SetTrigger("attack");
        }
        LifeEnemy_UI.Instance.DecreaseProgress(0.25f);
        Gameplay_UI.Instance.ResetProgress();
        GameManager.Instance.damage = 0;
    }

    void CameraReturn()
    {
        AnimationManager.Instance.player.SetBool("attack", false);
        movCamera = false;
        takeDmg = false;
        if (!movCamera)
        {
            Vector3 targetPosition = new Vector3(cameraObj.transform.position.x, targetPos.y - 0.68f, -10);
            cameraObj.transform.position = Vector3.SmoothDamp(cameraObj.transform.position, targetPosition, ref velocity, smoothTime);
        }
    }
}
