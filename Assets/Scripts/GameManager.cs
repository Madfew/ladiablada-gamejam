﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour 
{
    public static GameManager Instance;

    public AudioSource musicMain;
    public bool startPlay;

    public GameObject[] beatScroller;
    public GameObject[] beatScroller2;
    public GameObject[] beatScroller3;
    public GameObject[] Luces;

    public int numberStage;
    private int maxPoints;

    public float goodValor;
    public float reallyGoodValor;
    public float perfectValor;
    public float mehValor;

    public float camara;

    public bool specialAct;
    public int damage;

    public GameObject tutorial;

    public bool presionoTecla;

    void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        numberStage = 1;

        maxPoints = 30;

        goodValor = 0.02f;
        reallyGoodValor = 0.05f;
        perfectValor = 0.1f;
        mehValor = 0.01f;

        camara = 2.5f;

        presionoTecla = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!startPlay)
        {
            if (Input.anyKeyDown)
            {
                tutorial.SetActive(false);
                startPlay = true;

                AnimationManager.Instance.player.SetBool("dance", true);
                AnimationManager.Instance.diablillo[0].SetTrigger("dance");
                AnimationManager.Instance.diablillo[1].SetTrigger("dance");
                AnimationManager.Instance.diablillo[2].SetTrigger("dance");
                AnimationManager.Instance.diablillo[3].SetTrigger("dance");

                beatScroller[0].GetComponent<BeatScroller>().hasStarted = true;
                //beatScroller2.hasStarted = true;
                Luces[0].SetActive(true);
                Luces[1].SetActive(true);
                Luces[2].SetActive(true);
                Luces[3].SetActive(true);
                Luces[4].SetActive(true);
                Luces[5].SetActive(true);
                Luces[6].SetActive(true);
                Luces[7].SetActive(true);
                Luces[8].SetActive(true);
                musicMain.Play();
            }
        }

        if(damage > maxPoints)
        {
            //LevelManager.Instance.RestartScene();
            specialAct = true;
        } else
        {
            specialAct = false;
        }

        if(numberStage == 2)
        {
            StartCoroutine(EsperarEjecutar(3f, SecondStage));
        } else if (numberStage == 3)
        {
            StartCoroutine(EsperarEjecutar(3f, ThirdStage));
        } else if(numberStage == 4)
        {
            StartCoroutine(EsperarEjecutar(3f, ForthyStage));
        }

    }

    public void NoteHit(int damageNote)
    {
        switch (damageNote)
        {
            case 4:
                damage += 2;
                break;
            case 3:
                damage += 3;
                break;
            case 2:
                damage += 5;
                break;
            case 1:
                damage += 1;
                break;
            default:
                print("Gilaso");
                break;
        }
        //Debug.Log("Hit on time");
    }

    public void NoteMissed()
    {
        damage--;
        if (damage < 0)
        {
            damage = 0;
        }
        //Debug.Log("Missed note");
    }

    IEnumerator EsperarEjecutar(float tiempo, Action accion)
    {
        yield return new WaitForSecondsRealtime(tiempo);
        accion();
    }

    void SecondStage()
    {
        maxPoints = 65;

        goodValor = 0.01f;
        reallyGoodValor = 0.025f;
        perfectValor = 0.05f;
        mehValor = 0.005f;

        beatScroller[1].GetComponent<BeatScroller>().hasStarted = true;
        beatScroller2[0].GetComponent<BeatScroller_2>().hasStarted = true;
        //beatScroller2.hasStarted = true;
    }

    void ThirdStage()
    {
        maxPoints = 90;

        goodValor = 0.009f;
        reallyGoodValor = 0.015f;
        perfectValor = 0.04f;
        mehValor = 0.004f;

        beatScroller[2].GetComponent<BeatScroller>().hasStarted = true;
        beatScroller3[0].GetComponent<BeatScroller_3>().hasStarted = true;
        //beatScroller3.hasStarted = true;
    }

    void ForthyStage()
    {
        camara = 4f;

        maxPoints = 115;

        goodValor = 0.005f;
        reallyGoodValor = 0.0125f;
        perfectValor = 0.025f;
        mehValor = 0.002f;

        beatScroller[3].GetComponent<BeatScroller>().hasStarted = true;
        beatScroller2[1].GetComponent<BeatScroller_2>().hasStarted = true;
        beatScroller3[1].GetComponent<BeatScroller_3>().hasStarted = true;
        //beatScroller3.hasStarted = true;
    }

}
